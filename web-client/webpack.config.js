const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");

module.exports = {
    entry: { index: path.resolve(__dirname, "src", "main.js") },
    output: { path: path.resolve(__dirname, "build") },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        historyApiFallback: true,
        open: true,
        port: 8080,
    },
    module: {
        rules: [
          {
            test: /\.s?css$/i,
            use: ["style-loader", "css-loader", "sass-loader", "postcss-loader"]
          },
          {
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: ["babel-loader"]
          }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
          template: path.resolve(__dirname, "src", "index.html"),
          favicon: "./src/assets/favicon.png",
        })
    ]
}