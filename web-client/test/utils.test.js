const { validateUrl } = require('../src/util/validators');

describe('URL Validator', () => {
    it('Should return true', () => {
        const url = 'http://www.google.com';
        const result = validateUrl(url);
        expect(result).toBeTruthy();
    })
    it('Should return true', () => {
        const url = 'http://google.com';
        const result = validateUrl(url);
        expect(result).toBeTruthy();
    })
    it('Should return true', () => {
        const url = 'https://www.google.com';
        const result = validateUrl(url);
        expect(result).toBeTruthy();
    })
    it('Should return false', () => {
        const url = 'http://bobsite';
        const result = validateUrl(url);
        expect(result).toBeFalsy();
    })
    it('Should return false', () => {
        const url = '';
        const result = validateUrl(url);
        expect(result).toBeFalsy();
    })
    it('Should return false', () => {
        let url;
        const result = validateUrl(url);
        expect(result).toBeFalsy();
    })
})