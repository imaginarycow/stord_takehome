import axios from 'axios';

export default class BabyURLAPI {

    static baseUrl = 'http://localhost:3000/api/urls';

    static createBabyUrl(longUrl) {
        return axios({
            method: 'post',
            url: this.baseUrl,
            data: { longUrl }
        })
        .then(response => response.data)
        .catch(err => {
            // log error to service, and retry
            console.log(err)
        })
    }

    static getBabyURLDetails(babyUrlSlug) {
        return axios.get(`${this.baseUrl}/${babyUrlSlug}`)
        .then(response => response.data)
        .catch(err => {
            // log error to service, and retry
            console.log(err)
        })
    }
}