import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import { getBabyUrl } from '../redux/urlActions';

export default () => {

    const dispatch = useDispatch();
    const { slug } = useParams();
    const [err, setErr] = useState();
    const [message, setMessage] = useState('Searching for your url...');
    const [redirect, setRedirect] = useState();
    const loading = useSelector(state => state.loading);

    useEffect(() => {
        if (slug) {
            dispatch(getBabyUrl(slug))
            .then(redirect => {
                setMessage('Redirecting...')
                setRedirect(redirect.originalUrl)
            })
            .catch(err => {
                setErr(err);
                setMessage('We looked everywhere and could not find what you were looking for. Please try again.')
            })
        }
    }, [slug])

    if (redirect) return window.location.href = redirect;

    return (
        <main style={{ display: 'block', marginTop: '10%' }}>
            <h1 className="title">{message}</h1>
            {
                err ? (
                    <button className="fancy-button" onClick={() => setRedirect('/')}>Or, lets just go make a BabyUrl!</button>
                ) : null
            }
        </main>
    )
}