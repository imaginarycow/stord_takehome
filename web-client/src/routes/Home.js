import ShortenerForm from '../components/ShortenerForm';

export default () => (
    <main>
        <ShortenerForm />
    </main>
)