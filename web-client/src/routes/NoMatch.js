import { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';

export default () => {

    const [timer, setTimer] = useState(7);

    useEffect(() => {
        const interval = setInterval(() => {
            setTimer(timer - 1);
        }, 1000)

        return () => clearInterval(interval)
    }, [timer])

    if (timer === 0) return <Redirect to='/' />;

    return (
        <main style={{ marginTop: '10%' }}>
            <h1 className="title">Hmmm, looks like you may be lost. Redirecting in {timer}</h1>
        </main>
    )
}