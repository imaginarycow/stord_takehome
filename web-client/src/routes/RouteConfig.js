import { Route, Switch } from 'react-router-dom';

import BabyUrlRedirect from './BabyUrlRedirect';
import Home from './Home';
import MyLinks from './MyLinks';
import NoMatch from './NoMatch';

export default () => (
    <Switch>
        <Route path="/" exact component={Home} />
        <Route path="/:slug" exact component={BabyUrlRedirect} />
        <Route path="/my-links" exact component={MyLinks} />
        <Route component={NoMatch} />
    </Switch>
)