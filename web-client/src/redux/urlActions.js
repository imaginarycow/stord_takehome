import BabyUrlApi from '../api/BabyUrlAPI';

export function createBabyUrl(longUrl) {
    return dispatch => {
        dispatch({
            type: 'SET_LOADING',
            payload: true
        })
        return BabyUrlApi.createBabyUrl(longUrl)
        .then(response => {
            dispatch({
                type: 'SET_URL',
                payload: response
            })
            dispatch({
                type: 'SET_MESSAGE',
                payload: {text: `Your BabyUrl for ${longUrl} has been created!`, type: 'SUCCESS'}
            })
            return response;
        }).catch(err => {
            dispatch({
                type: 'SET_ERROR',
                payload: 'Failed to fetch'
            })
            dispatch({
                type: 'SET_MESSAGE',
                payload: {text: `Your BabyUrl for ${longUrl} failed to be created.`, type: 'ERROR'}
            })
        }).finally(() => {
            dispatch({
                type: 'SET_LOADING',
                payload: false
            })
        })
    }
}

export function getBabyUrl(slug) {
    return dispatch => {
        dispatch({
            type: 'SET_LOADING',
            payload: true
        })
        return BabyUrlApi.getBabyURLDetails(slug)
        .then(response => response)
        .catch(err => {
            dispatch({
                type: 'SET_ERROR',
                payload: 'Failed to fetch'
            })
            dispatch({
                type: 'SET_MESSAGE',
                payload: {text: `We could not find your BabyUrl - ${slug}`, type: 'ERROR'}
            })
            return err;
        })
        .finally(() => {
            dispatch({
                type: 'SET_LOADING',
                payload: false
            })
        })
    }
}
