export default (state={}, action) => {
    switch(action.type) {
        case 'SET_LOADING':
            return { ...state, loading: action.payload };
        case 'SET_URL':
            return { ...state, url: action.payload };
        case 'SET_MESSAGE':
            return { ...state, message: action.payload };
        default:
            return state;
    }
}