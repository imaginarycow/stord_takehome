import { configureStore } from '@reduxjs/toolkit';
import urlReducer from './urlReducer';

export default configureStore({
  reducer: urlReducer,
})