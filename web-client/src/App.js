import './App.scss';
import { useSelector } from 'react-redux';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Header from './components/Header';
import Routes from './routes/RouteConfig';

export default () => {
    const message = useSelector(state => state.message);

    if (message) {
        if (message.type === 'SUCCESS') {
            toast.success(message.text)
        } 
        if (message.type === 'ERROR') {
            toast.error(message.text)
        }
    }

    return (
        <>
            <Header siteTitle="BabyURL"/>
            <ToastContainer />
            <Routes />
        </>
    )
}
