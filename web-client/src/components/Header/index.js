import './header.scss';
import { Link } from 'react-router-dom';
import SiteNav from '../SiteNav';

export default ({ siteTitle }) => {

    return (
        <header className="title">
            <Link to="/">{siteTitle}</Link>
            <SiteNav />
        </header>
    )
}