import Loader from "react-loader-spinner";
import './loader.scss';

export default () => {

    return (
        <Loader 
        className="loader" 
        type="Hearts" 
        color="#00BFFF" 
        height={180} 
        width={180} />
    );
  
}

