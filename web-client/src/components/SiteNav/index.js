import './siteNav.scss';
import { Link } from 'react-router-dom';

export default () => {

    return (
        <nav id="site-nav">
            <Link to="/my-links">My Links</Link>
        </nav>
    )
}