import './shortenerForm.scss';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Loader from '../Loader';
import { createBabyUrl } from '../../redux/urlActions';
import { validateUrl } from '../../util/validators';

export default () => {

    const dispatch = useDispatch();
    const loading = useSelector(state => state.loading);
    const [createdUrl, setCreatedUrl] = useState();
    const [userInput, setUserInput] = useState('');
    const [message, setMessage] = useState('');

    const handleChange = e => {
        setUserInput(e.target.value.trim());
        if (message) setMessage('');
    }

    const handleSubmit = e => {
        e.preventDefault();

        if (validateUrl(userInput)) {
            dispatch(createBabyUrl(userInput))
            .then(babyUrl => setCreatedUrl(babyUrl));
        } else {
            setMessage(' * Please enter a valid URL (ex: https://www.site.com)')
        }
    }

    const resetForm = () => {
        setCreatedUrl();
        setMessage('');
        setUserInput('');
    }

    if (loading) return <Loader />;

    if (createdUrl) {
        let _babyUrl = `${createdUrl.host}/${createdUrl.slug}`;
        return (
            <div className="success-message">
                <h1 className="title">Congratulations on Your New BabyURL!!!</h1>
                <h2 className="title">Use <a href={_babyUrl}>{_babyUrl}</a> to get to:</h2>
                <h3 className="title">{createdUrl.originalUrl}</h3>
                <button className="fancy-button" onClick={resetForm}>Hey, Lets Make Another One!</button>
            </div>
        )
    }

    return (
        <form id="url-shortener-form" onSubmit={handleSubmit}>
            <label htmlFor="long-url">Enter a long URL to make a BabyURL</label>
            <p className="message">{message}</p>
            <input type="text" name="long-url" value={userInput} onChange={handleChange}/>
            <button className="fancy-button">Make a Baby(URL that is)!</button>
        </form>
    )
}