# STORD URL Shortener Exercise
The goal of this exercise is to create a URL shortener web application in the same vein as bitly, TinyURL, or the now defunct Google URL Shortener. It is intentionally open-ended and you are welcome to implement your solution using the language and tech stack of your choice (if you know React and/or Elixir well, then please use them for your submission). The core functionality of the application should be expressed through your own original code. 
Although this project is small in scope, it is a great opportunity for you to show off your fullstack skills in: attention to detail, testing, CI/CD, design and development. Show us what you got!
Application Requirements
When navigating to the root path (e.g. http://localhost:8080/) of the app in a browser a user should be presented with a form that allows them to paste in a (presumably long) URL (e.g. https://www.google.com/search?q=url+shortener&oq=google+u&aqs=chrome.0.69i59j69i60l3j0j69i57.1069j0j7&sourceid=chrome&ie=UTF-8).
When a user submits the form they should be presented with a simplified URL of the form http://localhost:8080/{slug} (e.g. http://localhost:8080/h40Xg2). The format and method of generation of the slug is up to your discretion.
When a user navigates to a shortened URL that they have been provided by the app (e.g. http://localhost:8080/h40Xg2) they should be redirected to the original URL that yielded that short URL (e.g https://www.google.com/search?q=url+shortener&oq=google+u&aqs=chrome.0.69i59j69i60l3j0j69i57.1069j0j7&sourceid=chrome&ie=UTF-8).
Only allow valid URLs (e.g., start with http(s)://{domain}/ )
Deliverables
Implement your solution, including test cases for your application code.
We will execute your code using either:
(a) the make targets specified in Makefile (download this file). 
Edit the contents of Makefile to provide an interface for running and testing your application
