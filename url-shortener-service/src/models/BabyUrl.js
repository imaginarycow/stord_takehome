const mongoose = require("mongoose");

const schema = mongoose.Schema({
    host: String,
	originalUrl: String,
    slug: String,
    date_created: Date
})

module.exports.default = mongoose.model("BabyUrl", schema)