const express = require('express');
const cors = require('cors');
const app = express();
const port = 3000;

const db = require('./data/database');

app.use(cors())
app.use(express.json());

app.get('/api/urls/:slug', async (req, res) => {

    const { slug } = req.params;

    if (slug) {
        try {
            let babyUrl = await db.getBabyUrl(slug);
            if (babyUrl) res.send(babyUrl);
            else {
                res.status(404);
                res.send('Check your params, and try again.')
            }
        } catch(err) {  
            res.status(400)
            res.send(err.message);
        }
    } else {
        res.status(400)
        res.send('Check your params, and try again.')
    }
});

app.post('/api/urls', async (req, res) => {
    const { longUrl } = req.body;
    if (!longUrl) {
        res.status(400);
        res.send('Invalid Url');
        return;
    }

    try {
        const babyUrl = await db.createBabyUrl(longUrl);
        res.status(201);
        res.json(babyUrl);
    } catch(err) {
        res.status(400);
        res.send('Something went wrong, check your request and try again.')
    }
});

app.listen(port, () => {
  console.log(`URL Shortener listening at http://localhost:${port}`)
})