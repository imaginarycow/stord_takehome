const mongoose = require('mongoose');
const { nanoid } = require('nanoid')

const BabyUrl = require('../models/BabyUrl').default;

class DBConnection {

    constructor() {
        //Set up default mongoose connection
        const connectionString = 'mongodb://0.0.0.0/url-shortener';
        mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});

        //Get the default connection
        this.db = mongoose.connection;

        this.db.once('open', function() {
            console.log('connected to database***')
        });
    }

    async createBabyUrl(url) {
        const babyUrl = new BabyUrl({
            host: 'http://localhost:8080',
            originalUrl: url,
            slug: nanoid(8),
            date_created: Date.now()
        })
        
        return await babyUrl.save();
    }

    async getBabyUrl(slug) {
        return await BabyUrl.findOne({ slug })
    }

    async getOriginalUrl(slug) {
        return await BabyUrl.findOne({ slug })
    }
}

const instance = new DBConnection();
Object.freeze(instance);

module.exports = instance;