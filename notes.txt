Once this project is cloned:

1) run the command 'make setup'
2) when that is done, run 'make server'

If I were to spend more time on it, I absolutely would be 
writing tests, and adding new features, like user login, 
and allowing users to edit their links