.PHONY: $(MAKECMDGOALS)

# `make setup` will be used after cloning or downloading to fulfill
# dependencies, and setup the the project in an initial state.
# This is where you might download rubygems, node_modules, packages,
# compile code, build container images, initialize a database,
# anything else that needs to happen before your server is started
# for the first time
setup:
	cd ./web-client && npm i
	cd ./url-shortener-service && npm i

#	docker stop mongodb2 && docker rm mongodb2
	docker-compose up -d && exit &&\
	docker exec mongodb2 bash && mongo use url-shortener
	
# `make server` will be used after `make setup` in order to start
# an http server process that listens on any unreserved port
#	of your choice (e.g. 8080). 
server:
	cd ./url-shortener-service && npm start&
	cd ./web-client && npm start
# `make test` will be used after `make setup` in order to run
# your test suite.
test:
	cd ./url-shortener-service && npm run test
	cd ./web-client && npm run test